import { ComponentFixture, TestBed } from '@angular/core/testing';

import {PubilcationComponent } from './pubilcation.component';

describe('PubilcationComponent', () => {
  let component:PubilcationComponent;
  let fixture: ComponentFixture<PubilcationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PubilcationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PubilcationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
