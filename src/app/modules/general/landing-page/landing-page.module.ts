import { IntroComponent } from './intro/intro.component';
import { StudentComponent } from './student/student.component';
import { LandingPageComponent } from './landing-page.component';
import { LandingPageRoutingModule } from './landing-page-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PubilcationComponent } from './pubilcation/pubilcation.component';
import { GralleryComponent } from './grallery/grallery.component';
import { NewsitComponent } from './newsit/newsit.component';
import { PositionComponent } from './position/position.component';
import {EntertainerComponent} from './entertainer/entertainer.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ContactComponent } from './contact/contact.component';
import { SliderComponent } from './slider/slider.component';
import { PortfolioComponent } from './portfolio/portfolio.component';

@NgModule({
  imports: [
    CommonModule,
    LandingPageRoutingModule,
    NgbModule
  ],
  exports: [
    LandingPageComponent
  ],
  declarations: [
    LandingPageComponent,
    PubilcationComponent,
    StudentComponent,
    IntroComponent,
    GralleryComponent,
    NewsitComponent,
    PositionComponent,
    ContactComponent,
    SliderComponent,
    PortfolioComponent,
    EntertainerComponent,
  ],
  providers: [
  ],
})
export class LandingPageModule { }