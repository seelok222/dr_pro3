import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsitComponent } from './newsit.component';

describe('NewsitComponent', () => {
  let component: NewsitComponent;
  let fixture: ComponentFixture<NewsitComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewsitComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
