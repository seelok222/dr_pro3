import { ContactComponent } from './contact/contact.component';
import { FooterComponent } from '../footer/footer.component';
import { GralleryComponent } from './grallery/grallery.component';
import { IntroComponent } from './intro/intro.component';
import { SliderComponent } from './slider/slider.component';
import {EntertainerComponent} from './entertainer/entertainer.component';
import { StudentComponent } from './student/student.component';
import { PubilcationComponent } from './pubilcation/pubilcation.component';
import { NewsitComponent } from './newsit/newsit.component';
import { PositionComponent } from './position/position.component';
import { LandingPageComponent } from './landing-page.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';





const routes: Routes = [
  { path: '', component: LandingPageComponent,
    children: [
      {
        path: ' slider',
        component: SliderComponent,
      },
      {
        path: ' grallery',
        component: GralleryComponent,
      },
      {
        path: 'pubilcation',
        component: PubilcationComponent,
      },
      {
        path: 'detail-landing',
        component: NewsitComponent,
      },
      {
        path: 'Position',
        component: PositionComponent,
      },
      {
        path: 'student',
        component: StudentComponent,
      },
      {
        path: 'contact',
        component: ContactComponent,
      },
      {
        path: 'intro',
        component: IntroComponent,
      },
      {
        path: 'entertainer',
        component: EntertainerComponent,
      },
      { path: '', redirectTo: 'facilities', pathMatch: 'full' },
      { path: '**', redirectTo: 'facilities', pathMatch: 'full' }
    ]
},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LandingPageRoutingModule { }