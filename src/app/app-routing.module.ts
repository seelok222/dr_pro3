import { LandingPageComponent } from './modules/general/landing-page/landing-page.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { NotFoundComponent } from './modules/general/not-found/not-found.component';

const routes: Routes = [
  { path: '', component: LandingPageComponent, },
  {
    path: 'landing-page',
    loadChildren: () => import('./modules/general/landing-page/landing-page.module')
      .then(mod => mod.LandingPageModule)
  },
  {
    path: 'student-detail',
    loadChildren: () => import('./modules/general/student-detail/student-detail.module')
      .then(mod => mod.StudentDetailModule)
  },
  {
    path: 'news-detail',
    loadChildren: () => import('./modules/general/news-detail/news-detail.module')
      .then(mod => mod.NewsDetailModule)
  },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled'
})],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }